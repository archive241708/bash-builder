#!/usr/bin/env bash

# Running in a container

set -euo pipefail

if [[ ! "$*" =~ --no-setup ]]; then
    # For local tests where the setup and clone has already been done:
    #
    # docker run -v /usr/bin/hd:/usr/bin/hd \
    #            -v "$PWD:/bash-builder"
    #            -v /lib/x86_64-linux-gnu/libbsd.so.0:/lib/x86_64-linux-gnu/libbsd.so.0 \
    #            --rm ubuntu:18.04 \
    #            bash -c "cd /bash-builder; bash gitlab-ci/run-test.sh --no-setup"

    apt-get update
    apt-get install -y git bsdmainutils

    cp -r bin/ bin-candidates/

    git clone https://gitlab.com/taikedz/bash-libs.git bash-libs
fi

BBPATH="$PWD/bash-libs/libs" bash test-integration/test-all.sh
