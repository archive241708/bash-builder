#!/usr/bin/env bash

### Install Bash Builder Usage:help
#
#   ./install.sh [OPTIONS]
#
# Install the build utilities.
#
# OPTIONS
#
# --libs
#   Also install the target version of the libraries, as configured in
#     libs.config file
#
# --rc
#   Install from the release candidates directory instead of release directory
#     (for development/testing purposes)
#
###/doc

#%include std/safe.sh
#%include std/autohelp.sh
#%include std/out.sh
#%include std/config.sh

#%include app/git.sh

main() {
    autohelp:check "$@"

    bindir="$HOME/.local/bin"
    libsdir="$HOME/.local/lib/bash-builder"
    INSTALL_SOURCE=bin/

    parse_args "$@"

    if [[ "$UID" = 0 ]]; then
        bindir="/usr/local/bin"
        libsdir="/usr/local/lib/bash-builder"
    fi

    mkdir -p "$bindir"
    mkdir -p "$libsdir"

    cp "$INSTALL_SOURCE"/bbuild \
        "$INSTALL_SOURCE"/bbrun \
        "$INSTALL_SOURCE"/bashdoc \
        "$INSTALL_SOURCE"/tarshc \
        "$bindir/"
    ensure_bbpath

    out:info "Installed to '$bindir'."

    install_libs
}

parse_args() {
    local item

    for item in "$@"; do
        case "$item" in
        --libs)
            load_libs_target
            ;;
        --rc)
            INSTALL_SOURCE=bin-candidates/
            ;;
        *)
            out:fail "Unknown option '$item'"
            ;;
        esac
    done
}


load_libs_target() {
    [[ -e libs.config ]] || {
        out:info "Copying libs.config.defaults to libs.config"
        cp libs.config.defaults libs.config
    }

    config:declare LIBSCONFIG libs.config
    config:load LIBSCONFIG
}

install_libs() {
    local reply
    if [[ -z "${LIBSCONFIG_version:-}" ]]; then
        out:warn "You may need to install additional bash-builder libraries separately."
        return 0
    fi

    git:ensure "$LIBSCONFIG_url" "$LIBSCONFIG_path" || out:fail "Problem with $LIBSCONFIG_path (does it point to '$LIBSCONFIG_url' as configured in libs.config ?)"

    "$LIBSCONFIG_path/install.sh" "${LIBSCONFIG_version:-}"
}

ensure_bbpath() {
    bashrcpath="$HOME/.bashrc"

    if [[ "$UID" = 0 ]]; then
        bashrcpath="/etc/bash.bashrc"
    fi

    if ! grep '^export BBPATH=' -q "$bashrcpath" ; then
        echo "export BBPATH=$libsdir" >> "$bashrcpath"
    fi
}

main "$@"
