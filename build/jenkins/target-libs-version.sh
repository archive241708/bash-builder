#!/usr/bin/env bash

set -euo pipefail

_read_config() {
    grep -E "^$1=" "libs.config.defaults" | sed -r 's/^[^=]+=//'
}

load_libs_config() {
    LIBSCONF_version="$(_read_config version)"
    LIBSCONF_path="$(_read_config path)"
    LIBSCONF_url="$(_read_config url)"

    (
        cd "$LIBSCONF_path"
        set -x

        git pull "$LIBSCONF_url"
        git checkout "$LIBSCONF_version"
    )
}

load_libs_config
