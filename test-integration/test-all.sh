#!/usr/bin/env bash

##bash-libs: safe.sh @ bc7b2b18 (2.1.10)

### Safe mode Usage:bbuild
#
# Set global safe mode options
#
# * Script bails if a statement or command returns non-zero status
#   * except when in a conditional statement
# * Accessing a variable that is not set is an error, causing non-zero status of the operation
# * Prevents globs
# * If a component of a pipe fails, the entire pipe statement returns non-zero
#
# Splitting over spaces
# ---------------------
#
# By default, array assignments and `read` command split over any whitespace.
#
# You can deactivate space splitting using `safe:space-split off`
# and reactivate it using `safe:space-split on`
#
# Globs
# -------
#
# In safe mode, glob expansion like `ls .config/*` is turned off by default.
#
# You can turn glob expansion on with `safe:glob on`, and off with `safe:glob off`
#
###/doc

safe:space-split() {
    case "$1" in
    off)
        export IFS=$'\t\n'
        ;;
    on)
        export IFS=$' \t\n'
        ;;
    *)
        out:fail "API error: bad use of safe:split - must be 'on' or 'off' not '$1'"
        ;;
    esac
}

safe:glob() {
    case "$1" in
    off)
        set -f
        ;;
    on)
        set +f
        ;;
    *)
        out:fail "API error: bad use of safe:glob - must be 'on' or 'off' not '$1'"
        ;;
    esac
}

set -eufo pipefail

##bash-libs: syntax-extensions.sh @ bc7b2b18 (2.1.10)

### Syntax Extensions Usage:syntax
#
# Syntax extensions for bash-builder.
#
# You will need to import this library if you use Bash Builder's extended syntax macros.
#
# You should not however use the functions directly, but the extended syntax instead.
#
##/doc

### syntax-extensions:use FUNCNAME ARGNAMES ... Usage:syntax
#
# Consume arguments into named global variables.
#
# If not enough argument values are found, the first named variable that failed to be assigned is printed as error
#
# ARGNAMES prefixed with '?' do not trigger an error
#
# Example:
#
#   #%include std/out.sh
#   #%include std/syntax-extensions.sh
#
#   get_parameters() {
#       . <(syntax-extensions:use get_parameters INFILE OUTFILE ?comment -- "$@")
#
#       [[ -f "$INFILE" ]]  || out:fail "Input file '$INFILE' does not exist"
#       [[ -f "$OUTFILE" ]] || out:fail "Output file '$OUTFILE' does not exist"
#
#       [[ -z "$comment" ]] || echo "Note: $comment"
#   }
#
#   main() {
#       get_parameters "$@"
#
#       echo "$INFILE will be converted to $OUTFILE"
#   }
#
#   main "$@"
#
###/doc
syntax-extensions:use() {
    local argname arglist undef_f dec_scope argidx argone failmsg pos_ok
    
    dec_scope=""
    [[ "${SYNTAXLIB_scope:-}" = local ]] || dec_scope=g
    arglist=(:)
    argone=\"\${1:-}\"
    pos_ok=true
    
    for argname in "$@"; do
        [[ "$argname" != -- ]] || break
        [[ "$argname" =~ ^(\?|\*)?[0-9a-zA-Z_]+$ ]] || out:fail "Internal: Not a valid argument name '$argname'"

        arglist+=("$argname")
    done

    argidx=1
    while [[ "$argidx" -lt "${#arglist[@]}" ]]; do
        argname="${arglist[$argidx]}"
        failmsg="\"Internal: could not get '$argname' in function arguments\""
        posfailmsg="\"Internal: positional argument '$argname' encountered after optional argument(s)\""

        if [[ "$argname" =~ ^\? ]]; then
            echo "$SYNTAXLIB_scope ${argname:1}"
            echo "${argname:1}=$argone; shift || :"
            pos_ok=false

        elif [[ "$argname" =~ ^\* ]]; then
            if [[ "$pos_ok" = true ]]; then
                echo "[[ '${argname:1}' != \"$argone\" ]] || out:fail \"Internal: Local name [${argname:1}] is the same is the name it tries to reference. Rename [$argname] (suggestion: [*p_${argname:1}])\""
                echo "declare -n${dec_scope} ${argname:1}=$argone; shift || out:fail $failmsg"
            else
                echo "out:fail $posfailmsg"
            fi

        else
            if [[ "$pos_ok" = true ]]; then
                echo "$SYNTAXLIB_scope ${argname}"
                echo "${argname}=$argone; shift || out:fail $failmsg"
            else
                echo "out:fail $posfailmsg"
            fi
        fi

        argidx=$((argidx + 1))
    done
}


### syntax-extensions:use:local FUNCNAME ARGNAMES ... Usage:syntax
# 
# Enables syntax macro: function signatures
#   e.g. $%function func(var1 var2) { ... }
#
# Build with bbuild to leverage this function's use:
#
#   #%include out.sh
#   #%include syntax-extensions.sh
#
#   $%function person(name email) {
#       echo "$name <$email>"
#
#       # $1 and $2 have been consumed into $name and $email
#       # The rest remains available in $* :
#       
#       echo "Additional notes: $*"
#   }
#
#   person "Jo Smith" "jsmith@example.com" Some details
#
###/doc
syntax-extensions:use:local() {
    SYNTAXLIB_scope=local syntax-extensions:use "$@"
}

args:use:local() {
    syntax-extensions:use:local "$@"
}
##bash-libs: tty.sh @ bc7b2b18 (2.1.10)

### tty.sh Usage:bbuild
# Get information on the current terminal session.
###/doc

### tty:is_ssh Usage:bbuild
# Determine whether the TTY is an SSH session.
#
# WARNING: this only works for an SSH connection still in the "landing" account.
# If the user is switched via 'su' or 'sudo', the environment is lost and the variables used to determine this are blank - by default, indicating being not in an SSH session.
###/doc
tty:is_ssh() {
    [[ -n "${SSH_TTY:-}" ]] || [[ -n "${SSH_CLIENT:-}" ]] || [[ "${SSH_CONNECTION:-}" ]]
}

### tty:is_pipe Usage:bbuild
# Determine if we are running in a pipe.
###/doc
tty:is_pipe() {
    [[ ! -t 1 ]]
}

### tty:is_multiplexer Usage:bbuild
# Determine if we are in a terminal multiplexer (detects 'screen' and 'tmux')
###/doc
tty:is_multiplexer() {
    [[ -n "${TMUX:-}" ]] || [[ "${TERM:-}" = screen ]]
}

##bash-libs: colours.sh @ bc7b2b18 (2.1.10)

### Colours for terminal Usage:bbuild
# A series of shorthand colour flags for use in outputs, and functions to set your own flags.
#
# Not all terminals support all colours or modifiers.
#
# Example:
# 	
# 	echo "${CRED}Some red text ${CBBLU} some blue text. $CDEF Some text in the terminal's default colour")
#
# Preconfigured colours available:
#
# CRED, CBRED, HLRED -- red, bright red, highlight red
# CGRN, CBGRN, HLGRN -- green, bright green, highlight green
# CYEL, CBYEL, HLYEL -- yellow, bright yellow, highlight yellow
# CBLU, CBBLU, HLBLU -- blue, bright blue, highlight blue
# CPUR, CBPUR, HLPUR -- purple, bright purple, highlight purple
# CTEA, CBTEA, HLTEA -- teal, bright teal, highlight teal
# CBLA, CBBLA, HLBLA -- black, bright red, highlight red
# CWHI, CBWHI, HLWHI -- white, bright red, highlight red
#
# Modifiers available:
#
# CBON - activate bright
# CDON - activate dim
# ULON - activate underline
# RVON - activate reverse (switch foreground and background)
# SKON - activate strikethrough
# 
# Resets available:
#
# CNORM -- turn off bright or dim, without affecting other modifiers
# ULOFF -- turn off highlighting
# RVOFF -- turn off inverse
# SKOFF -- turn off strikethrough
# HLOFF -- turn off highlight
#
# CDEF -- turn off all colours and modifiers(switches to the terminal default)
#
# Note that highlight and underline must be applied or re-applied after specifying a colour.
#
# If the session is detected as being in a pipe, colours will be turned off.
#   You can override this by calling `colours:check --color=always` at the start of your script
#
###/doc

### colours:check ARGS ... Usage:bbuild
#
# Check the args to see if there's a `--color=always` or `--color=never`
#   and reload the colours appropriately
#
#   main() {
#       colours:check "$@"
#
#       echo "${CGRN}Green only in tty or if --colours=always !${CDEF}"
#   }
#
#   main "$@"
#
###/doc
colours:check() {
    if [[ "$*" =~ --color=always ]]; then
        COLOURS_ON=true
    elif [[ "$*" =~ --color=never ]]; then
        COLOURS_ON=false
    fi

    colours:define
    return 0
}

### colours:set CODE Usage:bbuild
# Set an explicit colour code - e.g.
#
#   echo "$(colours:set "33;2")Dim yellow text${CDEF}"
#
# See SGR Colours definitions
#   <https://en.wikipedia.org/wiki/ANSI_escape_code#SGR_(Select_Graphic_Rendition)_parameters>
###/doc
colours:set() {
    # We use `echo -e` here rather than directly embedding a binary character
    if [[ "$COLOURS_ON" = false ]]; then
        return 0
    else
        echo -en "\033[${1}m"
    fi
}

### colours:pipe CODE Usage:bbuild
#
# Prints a colourisation byte sequence using the provided number, then writes the pipe stream, and then writes a reset sequence (default is '0' to reset colours).
#
###/doc
colours:pipe() {
    . <(args:use:local colourint ?reset -- "$@") ; 
    if [[ "$COLOURS_ON" = true ]]; then
        colours:set "$colourint"
        cat -
        colours:set "${reset:-0}"
    fi
}

colours:define() {

    # Shorthand colours

    export CBLA="$(colours:set "30")"
    export CRED="$(colours:set "31")"
    export CGRN="$(colours:set "32")"
    export CYEL="$(colours:set "33")"
    export CBLU="$(colours:set "34")"
    export CPUR="$(colours:set "35")"
    export CTEA="$(colours:set "36")"
    export CWHI="$(colours:set "37")"

    export CBBLA="$(colours:set "1;30")"
    export CBRED="$(colours:set "1;31")"
    export CBGRN="$(colours:set "1;32")"
    export CBYEL="$(colours:set "1;33")"
    export CBBLU="$(colours:set "1;34")"
    export CBPUR="$(colours:set "1;35")"
    export CBTEA="$(colours:set "1;36")"
    export CBWHI="$(colours:set "1;37")"

    export HLBLA="$(colours:set "40")"
    export HLRED="$(colours:set "41")"
    export HLGRN="$(colours:set "42")"
    export HLYEL="$(colours:set "43")"
    export HLBLU="$(colours:set "44")"
    export HLPUR="$(colours:set "45")"
    export HLTEA="$(colours:set "46")"
    export HLWHI="$(colours:set "47")"

    # Modifiers
    
    export CBON="$(colours:set "1")"
    export CDON="$(colours:set "2")"
    export ULON="$(colours:set "4")"
    export RVON="$(colours:set "7")"
    export SKON="$(colours:set "9")"

    # Resets

    export CBNRM="$(colours:set "22")"
    export HLOFF="$(colours:set "49")"
    export ULOFF="$(colours:set "24")"
    export RVOFF="$(colours:set "27")"
    export SKOFF="$(colours:set "29")"

    export CDEF="$(colours:set "0")"

}

colours:auto() {
    if tty:is_pipe ; then
        COLOURS_ON=false
    else
        COLOURS_ON=true
    fi

    colours:define
    return 0
}

colours:auto

##bash-libs: out.sh @ bc7b2b18 (2.1.10)

### Console output handlers Usage:bbuild
#
# Write data to console stderr using colouring
#
###/doc

### out:info MESSAGE Usage:bbuild
# print a green informational message to stderr
###/doc
function out:info {
    echo "$CGRN$*$CDEF" 1>&2
}

### out:warn MESSAGE Usage:bbuild
# print a yellow warning message to stderr
###/doc
function out:warn {
    echo "${CBYEL}WARN: $CYEL$*$CDEF" 1>&2
}

### out:defer MESSAGE Usage:bbuild
# Store a message in the output buffer for later use
###/doc
function out:defer {
    OUTPUT_BUFFER_defer[${#OUTPUT_BUFFER_defer[@]}]="$*"
}

# Internal
function out:buffer_initialize {
    OUTPUT_BUFFER_defer=(:)
}
out:buffer_initialize

### out:flush HANDLER ... Usage:bbuild
#
# Pass the output buffer to the command defined by HANDLER
# and empty the buffer
#
# Examples:
#
# 	out:flush echo -e
#
# 	out:flush out:warn
#
# (escaped newlines are added in the buffer, so `-e` option is
#  needed to process the escape sequences)
#
###/doc
function out:flush {
    [[ -n "$*" ]] || out:fail "Did not provide a command for buffered output\n\n${OUTPUT_BUFFER_defer[*]}"

    [[ "${#OUTPUT_BUFFER_defer[@]}" -gt 1 ]] || return 0

    for buffer_line in "${OUTPUT_BUFFER_defer[@]:1}"; do
        "$@" "$buffer_line"
    done

    out:buffer_initialize
}

### out:fail [CODE] MESSAGE Usage:bbuild
# print a red failure message to stderr and exit with CODE
# CODE must be a number
# if no code is specified, error code 127 is used
###/doc
function out:fail {
    local ERCODE=127
    local numpat='^[0-9]+$'

    if [[ "$1" =~ $numpat ]]; then
        ERCODE="$1"; shift || :
    fi

    echo "${CBRED}ERROR FAIL: $CRED$*$CDEF" 1>&2
    exit $ERCODE
}

### out:error MESSAGE Usage:bbuild
# print a red error message to stderr
#
# unlike out:fail, does not cause script exit
###/doc
function out:error {
    echo "${CBRED}ERROR: ${CRED}$*$CDEF" 1>&2
}
##bash-libs: patterns.sh @ bc7b2b18 (2.1.10)

### Useful patterns Usage:bbuild
#
# Some useful regex patterns, exported as environment variables.
#
# They are not foolproof, and you are encouraged to improve upon them.
#
# $PAT_blank - detects whether an entire line is empty or whitespace
# $PAT_comment - detects whether is a line is a script comment (assumes '#' as the comment marker)
# $PAT_num - detects whether the string is an integer number in its entirety
# $PAT_cvar - detects if the string is a valid C variable name
# $PAT_filename - detects if the string is a safe UNIX or Windows file name;
#   does not allow presence of special characters aside from '_', '.', and '-'
# $PAT_email - simple heuristic to determine whether a string looks like a valid email address
#
###/doc

export PAT_blank='^\s*$'
export PAT_comment='^\s*(#.*)?$'
export PAT_num='^[0-9]+$'
export PAT_cvar='^[a-zA-Z_][a-zA-Z0-9_]*$'
export PAT_filename='^[a-zA-Z0-9_. -]+$'
export PAT_email="[a-zA-Z0-9_.+-]+@[a-zA-Z0-9][a-zA-Z0-9.-]+\.[a-z]+"

##bash-libs: args.sh @ bc7b2b18 (2.1.10)

### args Usage:bbuild
#
# An arguments handling utility.
#
###/doc

### args:get TOKEN ARGS ... Usage:bbuild
#
# Given a TOKEN, find the argument value
#
# Typically called with the parent's arguments
#
# 	args:get --key "$@"
# 	args:get -k "$@"
#
# If TOKEN is an int, returns the argument at that index (starts at 1, negative numbers count from end backwards)
#
# If TOKEN starts with two dashes ("--"), expect the value to be supplied after an equal sign
#
# 	--token=desired_value
#
# If TOKEN starts with a single dash, and is a letter or a number, expect the value to be the following token
#
# 	-t desired_value
#
# Returns 1 if could not find anything appropriate.
#
###/doc

args:get() {
    local seek="$1"; shift || :

    if [[ "$seek" =~ $PAT_num ]]; then
        local arguments=("$@")

        # Get the index starting at 1
        local n=$((seek-1))
        # but do not affect wrap-arounds
        [[ "$n" -ge 0 ]] || n=$((n+1))

        echo "${arguments[$n]}"

    elif [[ "$seek" =~ ^--.+ ]]; then
        args:get_long "$seek" "$@"

    elif [[ "$seek" =~ ^-[a-zA-Z0-9]$ ]]; then
        args:get_short "$seek" "$@"

    else
        return 1
    fi
}

args:get_short() {
    local token="$1"; shift || :
    while [[ -n "$*" ]]; do
        local item="$1"; shift || :

        if [[ "$item" = "$token" ]]; then
            echo "$1"
            return 0
        fi
    done
    return 1
}

args:get_long() {
    local token="$1"; shift || :
    local tokenpat="^$token=(.*)$"

    for item in "$@"; do
        if [[ "$item" =~ $tokenpat ]]; then
            echo "${BASH_REMATCH[1]}"
            return 0
        fi
    done
    return 1
}

### args:has TOKEN ARGS ... Usage:bbuild
#
# Determines whether TOKEN is present on its own in ARGS
#
# Typically called with the parent's arguments
#
# 	args:has thing "$@"
#
# Returns 0 on success for example
#
# 	args:has thing "one" "thing" "or" "another"
#
# Returns 1 on failure for example
#
# 	args:has thing "one thing" "or another"
#
# "one thing" is not a valid match for "thing" as a token.
#
###/doc

args:has() {
    local token="$1"; shift || :
    for item in "$@"; do
        if [[ "$token" = "$item" ]]; then
            return 0
        fi
    done
    return 1
}

### args:after TOKEN ARGS ... Usage:bbuild
#
# Return all tokens after TOKEN via the RETARR_ARGSAFTER
#
#    myargs=(one two -- three "four and" five)
# 	args:after -- "${myargs[@]}"
#
# 	for a in "${RETARR_ARGSAFTER}"; do
# 		echo "$a"
# 	done
#
# The above prints
#
# 	three
# 	four and
# 	five
#
###/doc

args:after() {
    local token="$1"; shift || :
    
    local current_token="$1"; shift || :
    while [[ "$#" -gt 0 ]] && [[ "$current_token" != "$token" ]]; do
        current_token="$1"; shift || :
    done

    RETARR_ARGSAFTER=("$@")
}

### args:quote ARGS ... Usage:bbuild
#
# Produce a single string with each argument. If the argument has whitespace, double-quotes are added. If the argument has double-quotes as part of its data, these are escaped.
#
###/doc
args:quote() {
    local argstring arg
    argstring=""

    for arg in "$@"; do
        arg="$(echo -n "$arg" | sed -rz 's/"/\\"/g ; s/\n/\\n/g ; s/\r/\\r/g ; s/\t/\\t/g')"

        if (echo "$arg" | grep -Pq '\s') || [[ -z "$arg" ]]; then
            arg="\"${arg}\""
        fi

        argstring="$argstring $arg"
    done

    echo "${argstring:1}"
}

##bash-libs: test.sh @ bc7b2b18 (2.1.10)

### Tests library Usage:bbuild
#
# Library for testing functions
#
# Example:
#
# 	isgreater() {
# 		echo "Checking $1 > $2"
# 		[[ "$1" -gt "$2" ]]
# 	}
#
# 	test:require isgreater 5 3
# 	test:forbid isgreater 3 5
# 	test:forbid isgreater 5 5
#
# 	test:report
#
###/doc

TEST_testfailurecount=0
TEST_testsran=0

test:ok() {
    echo "${CBGRN} OK ${CDEF} $*"
    TEST_testsran=$((TEST_testsran+1))
}

test:fail() {
    echo "${CBRED}FAIL${CDEF} $*"
    TEST_testsran=$((TEST_testsran+1))
    TEST_testfailurecount=$((TEST_testfailurecount+1))
}

### test:require COMMAND ARGS ... Usage:bbuild
#
# Runs the command with arguments; if the command returns 0 then success
#
# If the command returns non-zero, then a failure is reported, and the
#  command's output is printed
#
###/doc

test:require() {
    local result=:
    local res=0
    result="$("$@")" || res="$?"
    if [[ "$res" = 0 ]] ; then
        test:ok "REQUIRE: [ $(args:quote "$@") ]"
        if [[ "${ECHO_OK:-}" = true ]]; then
            echo -n "$result"|hd
        fi
    else
        test:fail "REQUIRE: [ $(args:quote "$@") ]"
        echo -n "$result"|hd
    fi
}

### test:require COMMAND ARGS ... Usage:bbuild
#
# Runs the command with arguments; if the command returns 0 then a failure
#  is reported, and the command's output is printed
#
# If the command returns non-zero, then success
#
###/doc

test:forbid() {
    local result=:
    local res=0
    result="$("$@")" || res="$?"
    if [[ "$res" = 0 ]] ; then
        test:fail "FORBID : [ $(args:quote "$@") ]"
        echo -n "$result" | hd
    else
        test:ok "FORBID : [ $(args:quote "$@") ]"
        if [[ "${ECHO_OK:-}" = true ]]; then
            echo -n "$result"|hd
        fi
    fi
}

### test:report Usage:bbuild
#
# Report the number of tests ran, and failed
#
##/doc

test:report() {
    local reportcmd=out:info
    if [[ "$TEST_testsran" -lt 1 ]]; then
        reportcmd=out:warn
        TEST_testfailurecount=1

    elif [[ "$TEST_testfailurecount" -gt 0 ]]; then
        reportcmd=out:error
    fi

    "$reportcmd" "[$(basename "$0")] --- Ran $TEST_testsran tests with $TEST_testfailurecount failures"

    return "$TEST_testfailurecount"
}

### test:output EXPECTED COMMAND ... Usage:bbuild
# Test a command's output for expected content.
#
# Runs COMMAND ... and captures output. If the output matches the EXPECTED value, returns 0
# else returns 1
#
# Always prints the output ; combine with test:require and test:forbid
#
# *DEPRECATED* Use test:matches or test:differs instead.
#
###/doc
test:output() {
    out:warn "--- test:output is deprecated | use test:matches or test:differs ---"
    local expect="$1"; shift
    local output

    output="$("$@")" || :

    echo "$output"
    [[ "$expect" = "$output" ]]
}

### test:matches EXPECTED COMMAND ... Usage:bbuild
#
# Test a command's output for expected content.
#
# Runs COMMAND ... and captures output. If the output matches the EXPECTED value, registers a test success;
# else registers a test failure and prints the two results.
#
# Supercedes 'test:require test:output EXPECTED COMMAND ...'
#
###/doc
test:matches() {
    local expect="$1"; shift
    local output quoted

    output="$("$@")" || :
    quoted="$(args:quote "$@")"

    if [[ "$expect" = "$output" ]]; then
        test:ok "MATCHES: [ $quoted ]"
    else
        test:fail "MATCHES: [ $quoted ]"
        out:error "Expected:"
        echo -n "$expect" | hd
        out:error "But got:"
        echo -n "$output" | hd
        out:error "++++"
    fi
}

### test:differs ANTIEXPECTED COMMAND ... Usage:bbuild
#
# Test a command's output for anti-expected content.
#
# Runs COMMAND ... and captures output. If the output differs from the EXPECTED value, registers a test success;
# else registers a test failure and prints the data.
#
# Supercedes 'test:forbid test:output EXPECTED COMMAND ...'
#
###/doc
test:differs() {
    local antiexpect="$1"; shift
    local output quoted

    output="$("$@")" || :
    quoted="$(args:quote "$@")"

    if [[ "$antiexpect" != "$output" ]]; then
        test:ok "DIFFERS: [ $quoted ]"
    else
        test:fail "DIFFERS: [ $quoted ]"
        out:error "Data was:"
        echo -n "$output" | hd
        out:error "++++"
    fi
}

if ! which hd &>/dev/null; then
    out:fail "You need the 'hd' command for hexdump to use this test reporter."
fi
##bash-libs: abspath.sh @ bc7b2b18 (2.1.10)

### abspath:path RELATIVEPATH [ MAX ] Usage:bbuild
# Returns the absolute path of a file/directory
#
# MAX defines the maximum number of "../" relative items to process
#   default is 50
###/doc

function abspath:path {
    local workpath="$1" ; shift || :
    local max="${1:-50}" ; shift || :

    if [[ "${workpath:0:1}" != "/" ]]; then workpath="$PWD/$workpath"; fi

    workpath="$(abspath:collapse "$workpath")"
    abspath:resolve_dotdot "$workpath" "$max" | sed -r 's|(.)/$|\1|'
}

function abspath:collapse {
    echo "$1" | sed -r 's|/\./|/|g ; s|/\.$|| ; s|/+|/|g'
}

function abspath:resolve_dotdot {
    local workpath="$1"; shift || :
    local max="$1"; shift || :

    # Set a limit on how many iterations to perform
    # Only very obnoxious paths should fail
    local obnoxious_counter
    for obnoxious_counter in $(seq 1 $max); do
        # No more dot-dots - good to go
        if [[ ! "$workpath" =~ /\.\.(/|$) ]]; then
            echo "$workpath"
            return 0
        fi

        # Starts with an up-one at root - unresolvable
        if [[ "$workpath" =~ ^/\.\.(/|$) ]]; then
            return 1
        fi

        workpath="$(echo "$workpath"|sed -r 's@[^/]+/\.\.(/|$)@@')"
    done

    # A very obnoxious path was used.
    return 2
}

##bash-libs: this.sh @ bc7b2b18 (2.1.10)

### this: Info about the current command Usage:bbuild
#
# Get information about the current running app.
#
# Additional to the documented functions, there are three environment variables:
#
# * `THIS_basename` : the plain name of the currently running script
# * `THIS_dirname` : the absolute path to the script's containing directory
# * `THIS_scriptpath` : the absolute path to the script
#
###/doc

### this:bin Usage:bbuild
# The file name of the running script, without its path
###/doc
function this:bin {
    if [[ -n "${BBRUN_SCRIPT:-}" ]]; then
        basename "$BBRUN_SCRIPT"
    else
        basename "$0"
    fi
}

### this:bindir Usage:bbuild
# The absolute path of the directory in which the command is running
###/doc
function this:bindir {
    if [[ -n "${BBRUN_SCRIPT:-}" ]]; then
        dirname "$BBRUN_SCRIPT"
    else
        abspath:path "$(dirname "$0")"
    fi
}

THIS_basename="$(this:bin)"
THIS_dirname="$(this:bindir)"
THIS_scriptpath="$THIS_dirname/$THIS_basename"

TESTS=(
    include-count
    function-signature
    trap
    shebang
)

move_to_here() {
    cd "$THIS_dirname"

    bbuild_bin="$(abspath:path "$PWD/../bin-candidates")/bbuild"

    f:bbuild() {
        "$bbuild_bin" "$@"
    }

    out:info "f:bbuild = [$bbuild_bin]"
}

f:count() {
    local i="$1"; shift

    [[ "$("$@")" = "$i" ]]
}

t:trap() {
    . <(args:use:local scriptfile -- "$@") ; 
    test:require grep "Came out" -q <("$scriptfile")
}

t:function-signature() {
    . <(args:use:local scriptfile -- "$@") ; 
    test:require grep "Running" -q <("$scriptfile")
}

t:include-count() {
    . <(args:use:local scriptfile -- "$@") ; 
    test:require f:count 1 grep '##bash-libs: safe\.sh' -c "$scriptfile"
}

t:shebang() {
    . <(args:use:local scriptfile -- "$@") ; 
    test:require grep '#!/usr/bin/env bash' "$scriptfile"
}

main() {
    local testname

    move_to_here

    for testname in "${TESTS[@]}"; do
        test:require f:bbuild "src/$testname.sh" "run-${testname}.sh"

        "t:$testname" "./run-${testname}.sh"

        rm "run-${testname}.sh"
    done

    test:report
}

main "$@"
