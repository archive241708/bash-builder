#!/bin/bash

### CronDo Usage:help
#
# Run cron job line immediately.
#
# Add `:;` to the start of any line which you want to access via `crondo`
#
# For example, if you have the following in your crontab:
#
# 	* * * * * echo Hello
# 	* * * * * :; echo Goodbye
#
# You will only be offered the option of running the second command.
#
# (`:` is a command that does nothing; normally used as a placeholder.
# Check that this is the case on your system (that is has not been overridden by a command or alias: issuing the command `type :` should return `: is a shell builtin` ))
#
###/doc

#%include std/askuser.sh
#%include std/out.sh
#%include std/autohelp.sh

get_valid_cron_lines() {
    # Get a comma-separated list of lines, as that is what askuser:choose expects
    crontab -l | grep -P '^[ 0-9/*-]+?:;'|sed -r -e 's/^.+?:;//' -e 's/$/ , /' |sed -r -e 's/^ , //'
}

main() {
    autohelp:check "$@"
    local cronchoices commandline
    cronchoices="$(get_valid_cron_lines)"

    if [[ -z "$cronchoices" ]]; then
        out:fail "No cron lines to extract. Start a cron command with ':;' to make it available in crondo."
    fi

    commandline="$(askuser:choose "Choose a single cron command to run" $cronchoices )"

    if [[ -z "$commandline" ]]; then
        out:fail "Nothing to do."
    fi

    bash -c "$commandline"
}

main "$@"
